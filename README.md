# vue-imbb

[IMBB.metaVRse.de](https://imbb.metavrse.de/) = Immersive Media Bullshit Bingo
----
![screenshot](https://digitalpraxis.de/images/biger/projekt_imbb.jpg)

**[DEMO](https://imbb.metavrse.de/)**

The IMBB is a free web glossar and knowledge database of tutorials, hardware, software or whatever VR / AR / XR related.

`Mainly it's an Vue.js based search layer on an JSON import :)`

  IMBB is [my](http://digitalpraxis.de/) try to collect resources when  [experimenting and leraning WebXR](https://www.metavrse.de/) using 
      tools like the [Mozilla supported aframe.io](http://www.aframe.io/) or my 
      [360&deg; cam](https://theta360.com/). 
      More [WebXR on my [metaVRse blog](https://blog.metavrse.de/)
      where I pick stuff around AR / VR / MR / XR ... 
      or let's meet at the [WebXR Meetup in Berlin](http://webxr-berlin.de/)
    
    If you have something to share I missed contact me by info@metavrse.de
  

### npm

Install via npm:

```bash
git clone git@gitlab.com:riskieee/vue-imbb.git

npm install

npm run serve
```

Have FUN!